La falaise de Hauteroche appartient à la commune de Hauteroche qui la
“loue” à la FSGT depuis sa découverte en 1977. Elle est entièrement
équipée et entretenue par des bénévoles, des milliers d’heures depuis
1977 ! Tout ceci a quand même un coût (les broches, la perceuse, les
trajets …). Ce coût était jusqu’à présent partiellement couvert par la
vente du topo. Malheureusement celui-ci doit faire face à la
concurrence de topos ne participant à l’équipement de la falaise. Il
nous a donc paru plus simple de mettre ce topo disponible gratuitement
en ligne et de chercher un autre mode de financement. Il peut être
imprimé et diffusé sous réserve de la licence CC-by-NC-SA.
