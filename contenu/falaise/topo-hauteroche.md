# Topo Hauteroche

La falaise de Hauteroche appartient à la commune de Hauteroche qui la “loue” à la FSGT depuis sa découverte en 1977. Elle est entièrement équipée et entretenue par des bénévoles, des milliers d’heures depuis 1977 ! Tout ceci a quand même un coût (les broches, la perceuse, les trajets …). Ce coût était jusqu’à présent partiellement couvert par la vente du topo. Malheureusement celui-ci doit faire face à la concurrence de topos ne participant à l’équipement de la falaise. Il nous a donc paru plus simple de mettre ce topo disponible gratuitement en ligne et de chercher un autre mode de financement. Il peut être imprimé et diffusé sous réserve de la licence CC-by-NC-SA.

Le topo est disponible gratuitement en cliquant sur le lien ci-dessous.

- [Topo Hauteroche 2023 en pdf pour lire sur écran](../files/Topo-Hauteroche-04-02-23.pdf)
- [Topo Hauteroche 2023 en pdf pour impression en livret A5](../files/Topo-Hauteroche-livret-04-02-23.pdf)

Vous pouvez participer à l’équipement et l’entretien de la falaise en donnant un peu de votre temps ou de votre argent. Vous trouverez ci-dessous un lien vers une cagnotte en ligne.

Un grand merci à toutes les personnes qui ont déjà donné de leur temps pour entretenir la falaise, équiper et rééquiper les voies. Sans elles vous ne pourriez pas grimper aujourd’hui sur cette belle falaise. Profitez en bien, respectez le lieu et remportez tous vos déchets.

**Tous les bénévoles de la FSGT.**
