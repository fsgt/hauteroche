# Témoignage sur la gestion durable du site de Hauteroche

Le développement durable appliqué au sport de montagne et à l’escalade plus particulièrement doit répondre aux trois points de sa définition (justice sociale, progrès économique et protection de l’environnement. La CFME (Commission Fédérale Montagne Escalade) au sein de la FSGT (Fédération Sportive et Gymnique du Travail,) soutient les clubs à promouvoir un équilibre entre ces trois axes et participe depuis des années à la mise en place d’initiatives, parmi lesquelles la gestion de la falaise de Hauteroche, reposant sur 3 acteurs, est l’objet de notre témoignage.

– La Mairie, propriétaire du site, assure les relations territoriales et délègue à

– La FSGT, la gestion opérationnelle du site.

– La LPO (Ligue de Protection des Oiseaux) coordonne la sauvegarde d’une aire de reproduction du faucon pèlerin dans une zone regroupant 5 voies d’escalade. Un arrêté préfectoral sera mis en place au printemps 2018.

La FSGT, par son fonctionnement associatif essaye d’agir dans les trois axes :

Protection de l’environnement :

– Une aire de bivouac, (toilettes sèches, panneau d’affichage, zone délimitée pour faire des feux de camp et bientôt un composteur) aménagée proche de la falaise, mais très éloignée de la zone de nidification du faucon, incite à vivre l’escalade proche de la nature le temps d’un week-end. La légèreté des infrastructures (absence d’eau courante, accès à pied à la falaise, absence de confort coûteux en énergie) engendre un faible impact environnemental.

– Organisées sur le site-même, les formations d’Initiateur SNE, abordent un module spécifique à l’Eco-responsabilité.

– Annuellement, la Fête de la Falaise conviviale et éducative organisée au pied de la falaise, est également l’occasion d’une prise de conscience de la fragilité du milieu.

– L’éducation à l’environnement et à une pratique responsable même si elle a ses limites, semble porter ses fruits car depuis quelques années les grimpeurs ont compris qu’il n’était pas dans leur intérêt de laisser une falaise «sale» derrière eux et malgré une fréquentation grandissante, la falaise reste globalement très propre sans avoir eu besoin d’investir dans des poubelles ni un entretien coûteux du site, ni d’envisager un quelconque péage, montrant ainsi la faisabilité d’une pratique aux coûts (environnementaux et sociétaux) faibles.

La justice sociale : S’appuyant sur une vie associative prônant l’implication de chacun, la maintenance du site fait uniquement appel à la participation de bénévoles. Son coût ainsi minimisé combiné à un accès à des aménagements peu coûteux, permet à tous d’accéder gratuitement à cette pratique.

Le progrès économique : Même si son accès est gratuit, la falaise participe à la revitalisation de l’économie locale en attirant des grimpeurs et des touristes qui ont besoin de se loger, de se nourrir, de se déplacer (train, carburant), de se faire plaisir (artisanat local, restaurant). Et bien sûr la fête de la falaise organisée chaque année conjointement par des habitants du village et des grimpeurs d’Île de France ont des retombées économiques car les produits consommés sont généralement locaux.

Cependant des interrogations subsistent sur le plan de la durabilité :

– Une telle gestion résistera t-elle aux évolutions juridiques de la responsabilité des gestionnaires de sites (affaire de Vingrau) ?

– La normalisation des équipements de falaise et la surenchère sur le matériel sous prétexte de sécurité ne risquent-elles pas de démotiver les gestionnaires/équipeurs de falaise et l’augmentation des coûts de gestion des falaises de fragiliser l’équilibre actuel leurs financements?

– Combien de temps les efforts de sensibilisation et de responsabilisation resteront-ils à la hauteur d’une population grandissante de grimpeurs?

– Si la population des grimpeurs/consommateurs augmente trop par rapport à celle des grimpeurs autonomes et responsables, combien de temps l’action des bénévoles suffira t-elle?

– Même si les aménagements sont conçus de sorte à minimiser leur impact sur l’environnement, combien de temps le coût énergétique et environnemental de l’accès aux sites naturels restera t-il supportable?
