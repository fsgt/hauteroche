# Historique

## Quand la FSGT révolutionna l’escalade libre

Voilà plus de 40 ans que la falaise de Hauteroche, en Côte-d’Or, est
devenue, par l’action de la FSGT, un lieu reconnu de
l’escalade. Surtout, la conception pionnière – une grimpe accessible à
tous et toutes, en toute autonomie – de ce site s’est depuis imposée,
y compris à travers le monde, grâce à l’investissement associatif
continu des militant·es et bénévoles du sport populaire.

Dans les années soixante, l’escalade et l’alpinisme, autrefois
complémentaires, semblent en rupture. Pour repousser les limites
humaines, les alpinistes développent par exemple l’escalade
artificielle, tandis que d’autres parlent de «jaunir» les voies –
c’est-à-dire peindre en jaune les pitons ne devant plus être utilisés
comme points d’aide. Une escalade épurée dite «libre» voit le jour
avec comme éthique la formule suivante : grimper en tête, en libre et
à vue.

Dans ce contexte, des pionniers de l’escalade populaire FSGT décident
de franchir une nouvelle étape en posant clairement les termes
politiques de la diffusion de cette pratique. Ils ou elles défendent
le principe et la possibilité de la démocratisation face aux logiques
élitistes. Tout le monde doit pouvoir s’y adonner sans mettre sa vie
en danger. Regroupé·es derrière la Commission fédérale de montagne
(CFM) – qui rajoutera «escalade» plus tard (CFME) – ils et elles
reprenaient les conceptions élaborées par leurs aînés (André Koubbi,
Raymond Leininger ou Jean Vernet)[^1] : toutes les formes de pratiques
pour tous et toutes, grimper en responsable, à son niveau, en tête.


## Ouvrir une falaise «à l’aise»

C’est durant cette période charnière que le site de Hauteroche, en
Côte-d’Or, va être choisi, en 1977. Gilles Rotillon, membre du club de
Sainte-Geniève-des-Bois (91), lors d’une réunion de la CFM, émit
l’idée d’équiper une falaise selon les principes FSGT (ouverte à tous,
grimpe en tête à tous les niveaux…). Les camarades de Dijon en
dénichent une en moins de 15 jours : Hauteroche. Les autres
«géniteurs» s’appellent Guy Arnaud, président du club Amitié-Nature de
Dijon, appuyé par René Masson, maire entre 1954 à 1981, et Michelle
Arnaud, membre du collectif Montagne de la Côte-d’Or. Sans eux et
elle, cette belle aventure n’aurait pas existé.

Il s’agissait d’abord d’ouvrir une falaise dite «à l’aise», pour que
tout·e débutant·e puisse grimper en tête dans des voies faciles, en
toute sécurité. Aucun style d’escalade n’est imposé, contrairement à
la tendance dominante de l’époque (les voies faciles n’étaient pas
protégées, les voies moyennes étaient exposées et seules les voies
difficiles protégées). Hauteroche sera, ainsi, la première falaise en
France équipée selon des normes systématiques de protection de la
chute du grimpeur. Elle demeure en outre une des rares à offrir autant
de voies de faible niveau (une trentaine de voies de niveau 2 et 3
pour débutant·es). Certes les mentalités ont malgré tout
évolué. Toutes les falaises de l’Hexagone sont maintenant façonnées
pour l’essentiel selon des critères issus de cette première
expérimentation. Gilles Rotillon n’hésite pas d’ailleurs à revendiquer
que le modèle Hauteroche s’est également imposé comme «une norme
internationale». Une réalité que ce grand voyageur a pu observer, par
exemple, y compris sur le site réputé de Kalymnos en Grèce. Ce fut une
lente évolution. La FSGT n’en tira aucun bénéfice, aucune gloire.


## Un millier d’heures de travail bénévole

Mais ces convictions seraient restées lettres mortes sans le travail
et les actes de l’armée invisible de la montagne-escalade FSGT. Ici
commence la longue liste des bénévoles et des clubs FSGT qui vont
assurer l’entretien de la falaise. Une spécificité qui garde toute son
importance alors que la logique de professionnalisation gagne du
terrain. D’abord le gros du boulot est fourni par des adhérents du Red
Star Club de Champigny-sur-Marne (RSCC, 94) et également des grimpeurs
de Sainte-Geneviève-des-Bois (91) et de l’ASG Bagnolet (93). Au début
des années 80, René Finet du RSCC et la CFME entreprennent un
rééquipement plus fiable. Par la suite, Alain Finet, adhérent de la
section escalade du RSCC, sera pendant une quinzaine d’années le
cerveau et l’exécutant principal de l’évolution de la falaise. Il fut
aussi, avec l’aide de la Commission fédérale, l’auteur des trois
éditions du topo.

## Rééquipement de la falaise

En 1993, le RSCC passe la main (un millier d’heures de travail
bénévole et près de 20 000 francs investis en 15 ans) à Jean-Marc
Caglinni de l’US Ivry qui fournit un labeur important et méticuleux
pour terminer la sécurisation de la falaise. De nouvelles voies et des
variantes sont ouvertes. Pendant un temps le nettoyage est pris en
charge par les adhérent·es de Bobigny (93) tel Alain Lévèque
(aujourd’hui ES Stains), de Saint-Ouen (93) tel David Garnier ou de
Saint-Arnoult (78), avant que la commission escalade du Val-de-Marne
en assume le suivi de façon sporadique. Le chef d’orchestre d’alors
est Daniel Dupuis (USI) accompagné de Fabrice Duffault (US
Saint-Arnoult). Ainsi, toutes les bonnes volontés des clubs du
Val-de-Marne, et plus généralement les grimpeurs et grimpeuses FSGT
d’Ile-de-France, se regroupent pour participer aux week-ends
«nettoyage et équipement» ou aux modules d’équipement lors des
formations d’initiateurs·trices d’escalade FSGT.

Cette énumération peut sembler fastidieuse, mais elle est
indispensable pour comprendre à quel point cette expérience n’a pu
perdurer que grâce au dynamisme de la vie associative FSGT. Une vie
associative qui a des visages et des noms.


## La fête des falaises

La falaise se retrouve malheureusement un temps délaissée. En 2002,
après un accident de randonnée, la mairie contacte la FSGT pour
envisager une sécurisation de la zone. Une réunion a lieu sur place le
16 novembre 2002, entre Michel Andriot (maire), Brigitte Taupenot
(maire-adjointe et prof d’EPS) et Daniel Dupuis (FSGT 94). L’idée
d’une puis de deux journées découverte pour les habitant·es du
village, en 2003, est actée. Ces initiatives pousseront de jeunes
locaux motivé·es par cette activité sportive, à créer, en 2004,
l’association Hauteroche-Découverte. Celle-ci a pour objectif, entre
autres, de promouvoir l’escalade, de servir de relais entre la
municipalité et les responsables de de la CFME, d’assurer l’entretien
du site et de l’aire de repos (aire de bivouac) ainsi que des chemins
et des sentiers d’accès et de secours.

En 2006, débute l’écriture du nouveau topo par Bruno Poindron (ex
Grimpe 13 Paris puis USFontenay et Villejuif Altitude), rejoint par
Luc Barruel (RSCChampigny) et Rémi Cappeau (ex AS des Cheminots et
Villeneuvois 94, aujourd’hui membre de Grimpe 13). Ce dernier devient
un des acteurs essentiels de cette nouvelle «époque» d’Hauteroche. Il
a découvert la falaise au début des années 2000 et devient très
rapidement le chef d’orchestre de l’entretien. C’est grâce à son
investissement, et à celui des bénévoles qu’il a su motiver, que la
falaise est aujourd’hui autant utilisée et fréquentée toute
l’année. «Il y a des choses que je peux faire tout seul, d’autres pour
lesquelles j’ai besoin d’aide et celles que je fais avec d’autres pour
transmettre», raconte-t-il avec modestie.

Les relations avec le village et le monde des grimpeurs et grimpeuses
ont, elles aussi, pris une autre dimension. Les 30 ans de la falaise
d’escalade sont l’occasion pour Hauteroche-Découverte d’initier la
première «Fête des falaises» en mai 2007. Désormais annuelle, c’est un
moment convivial pour dire merci à tous les anonymes ayant contribué à
ce projet original et novateur. Elle permet de tisser des liens entre
les grimpeurs et la population locale, de réunir plus de 100 personnes
et de financer l’association pour l’entretien de l’aire de bivouac ou
des chemins d’accès.

Ces dernières années, la falaise est aussi le lieu de passage pour le
trail (course en pleine nature) de la Voie romaine au printemps. Les
chemins «de grimpeurs» font la joie des traileurs ou traileuses qui,
sans le savoir, participent par leur simple passage à leur entretien…
Des randonneurs et randonneuses s’y aventurent régulièrement pour
s’approcher au plus près de cette curiosité géologique. Une falaise
qui tend à redevenir omnisport, comme à ses débuts quand les
pratiquant·es du club Amitié Nature de Dijon s’en servaient comme
point de chute pour leurs diverses activités (randonnée, vélo, etc.),
d’où l’existence, dès le départ, d’une aire de bivouac aménagée. Si la
grimpe occupe toujours la place centrale, la falaise est mutualisée,
partagée, presque vivante !

Mais si elle semble épargnée par le temps, elle aura toujours besoin
de nouvelles énergies pour exister comme terrain de jeu. Alain Finet,
qui a ouvert à lui seul 98 voies, ne cache pas aujourd’hui son
émotion : «Ce qui fait plaisir c’est de voir des gens, qui n’étaient
pas nés au moment de la fondation de la falaise, se l’approprier,
l’entretenir… Un tel partage, sur une aussi longue durée, je ne vois
cela nul part ailleurs, ce feeling nature si particulier, qui continue
et même qui explose.»

[^1]: A. Koubbi : fondateur de la section montagne de l’US Ivry (94) ;
R. Leininger, issu du «Groupe de Bleau» (grimpeur de rochers de la
forêt de Fontainebleau) utilisera des techniques novatrices d’escalade
dans les années 1930 pour réaliser des ascensions de haut niveau,
comme la face nord des Drus ; J. Vernet, grand novateur de l’alpinisme
dans les années trente, notamment sans piton.
