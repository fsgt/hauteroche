# Financement de la falaise de Hauteroche

Vous trouverez ci-après un bilan du financement de l’entretien de la
falaise de Hauteroche depuis 2007, date de l’édition du dernier topo.


Dans un premier temps, faisons un résumé des années précédentes :

La première grande campagne d’équipement des années 80 a permis un
ré-équipement plus fiable. La commission fédérale FSGT
montagne-escalade et René FINET du club de Champigny sur Marne (RSCC
section Montagne Escalade Randonnée) prennent les choses en main et
entreprennent une campagne de ré-équipement avec des chevilles
autoforeuses (spits et autres) et des plaquettes. Un topo d’escalade
en noir et blanc est édité en 1979 avec 170 voies d’escalade
répertoriées. Une deuxième opération de ré-équipement, géré par Alain
FINET, démocratise la tige de tendeur comme point d’ancrage
« béton ». Les sommes ont été importantes pour acheter le matériel
(résine et tige tendeur) et ont principalement été payées par le club
de Champigny sur Marne. Un topo d’escalade en couleur est édité en
1985 avec 220 voies, puis une mise à jour en 1992 avec 235 voies. A
partir de 1993, le RSCC passe la main après un millier d’heures de
travail bénévole et avec près de 20 000 Francs investis en 15 ans. Une
troisième campagne voit le jour et permet l’installation systématique
des relais sommitaux munis de moulinox. C’est l’oeuvre de Jean Marc
CAGGLIGNY de l’US Ivry. L’informatique apparaît avec un tableau
numérique qui permet de faire le suivi précis de l’équipement. Depuis
seul un entretien régulier est nécessaire, même si une ou deux
nouvelles voies sont ouvertes chaque année depuis 2007 dans le cadre
d’une formation escalade SNE ou lors d’un week-end dédiés à
l’équipement.


Le bilan des stocks et de la vente des topos de Hauteroche :

Depuis sa ré-édition de 2007, le stock initial de 3000 topos avait été
prévu pour 15 ans, il reste environ 1300 topos à la fédération
fin 2017. On en a donc vendus (ou distribués lors des formations) 1700
en 10 ans. Les prévisions étaient de 1000 topos pour 5 ans. Elles ont
été optimistes, on a donc encore entre 6 et 7 ans de stock.


Les grandes campagnes très gourmandes en financement étant terminées
à Hauteroche, une falaise de 250 voies d’escalade a tout de même un
coût :

Il y a d’abord la location du couple falaise/aire de bivouac aménagée
qui est de l’ordre de 400€ (exactement 40 fois le smic horaire de
l’année en cours, amusez vous à faire le calcul) actuellement payée à
la commune par la fédération FSGT. Actuellement l’entretien ne coûte
pas cher, car le temps passé (et la presque totalité des transports)
est le fruit de l’investissement bénévole. Il n’en serait pas de même
s’il fallait rémunérer le temps passé et rembourser toutes les
dépenses. Les gros achats de matériel ont été financé par la
fédération FSGT, les comités départementaux et les clubs (20 broches
inox en 2017 et environ 200 broches en 2008 pour une somme avoisinant
les 450€). Le perforateur sans fil est personnel mais les batteries
sont un achat collectif de Grimpe 13, géré par la Coop Alpi et mis en
dépôt chez le gestionnaire pour garantir un bon entretien des
batteries.


Détaillons ici les autres moyens de financement de la falaise :

Une politique associative de la falaise permet à des grimpeurs de
s’investir dans l’entretien. Concrètement, l’organisation de week-ends
«nettoyage» ou de week-ends «équipement» où une partie du week-end est
consacrée à l’action bénévole et le reste à la grimpe
personnelle. Chacun y trouve son compte et donc cela ne nécessite pas
un remboursement systématique des participants. En plus cela réduit le
suivi des remboursements très chronophages pour le gestionnaire,
l’ouvreur du site et pour l’administration fédérale. Dans le cadre de
formation d’initiateur d’escalade SNE, une partie du temps de la
formation (temps bénévoles des formateurs et des stagiaires) et du
budget (entre 50 et 100€ pour acheter le matériel) permet d’améliorer
l’équipement ou d’ouvrir de nouvelles voies, cela permet aussi de
transmettre le savoir faire et au moins de sensibiliser les futurs
initiateurs des clubs à la gestion d’une falaise. Grâce à de très
bonnes relations, certains clubs parisiens ont, de temps en temps
quand c’est nécessaire, payé du matériel ou un voyage A/R au tarif
« essence+autoroute » pour soutenir l’équipement de la falaise. C’est
un choix volontaire issu d’une politique de club qui accepte de
consacrer une petite somme pour soutenir les ouvreurs/équipeurs pour
maintenir leur espace de grimpe en site naturel en bon état. Le
recyclage des cordes réformées (SAE ou SNE), dite « réforme de
confort » car la gaine a grossi et a du mal à passer dans les
descendeurs, en cordes d’équipement pour installer des rappels ou
monter du matériel ou faire une remonté sur corde, permet de limiter
considérablement l’achat de corde d’équipement. (précisons que le
matériel reste dans la norme des EPI) Il est toujours possible de
monter des dossiers de subvention auprès de la fédération FSGT, des
comités départementaux FSGT ou du conseil départemental. Le choix d’un
outillage robuste, non sophistiqué ou de récupération pour le
nettoyage permet de réduire le budget (scies, sécateurs, pioche, pied
de biche, broches, forets, chaîne, maillons rapides, etc…). Il est
assez facile de se faire prêter ponctuellement l’outillage plus
sophistiqué (tronçonneuse, perceuse sans fil, débroussailleuse, vérin
hydraulique, etc…). Tout bon équipeur y laisse inévitablement un peu,
voir beaucoup de temps. C’est inévitable mais aussi de l’argent car ça
lui tient à cœur, et c’est parfois plus simple pour les très petits
achats urgents ou ses frais de transport. L’organisation d’une fête
des falaises tous les ans, annoncées pour financer la gestion du site
(construction de toilette sèche, tonte de l’aire de bivouac,
tronçonnage d’arbre de temps en temps) attire du monde d’autant plus
si elle est sympathique. Une quette tous les ans au pied de la
falaise. Non là c’est une blague, mais sans blaguer cela a été proposé
pour solliciter ceux qui grimpent une fois dans leur vie sur cette
falaise, avec le vieux topo d’avant, sans licence… Mais peut être
faire une opération annuelle de souscription par internet avec des
outils adaptés. Le prix du topo va baisser en passant de 15€ à 10€ au
1er février 2018 pour faciliter la vente des 1300 derniers exemplaires
dans les 6 à 7 prochaines années. En enlevant les 3€ d’impression et
les 3€ du revendeurs, il «rapportera» 4€ par exemplaire vendu pour
financer l’équipement et la location de l’aire de bivouac aménagée. On
en vend environ 150 exemplaires par an, donc cela fait environ un
budget de 600€. Si on enlève les 400€ de la location de l’aire de
bivouac aménagée, il ne reste plus grand chose pour rembourser un ou
deux voyages depuis Paris ou du matériel. L’argent vient
inévitablement d’ailleurs.

D’ici à ce que le stock fédéral du topo de Hauteroche soit épuisé,
nous auront eu le temps de savoir ce qui sera le mieux pour la falaise
et ses grimpeurs :

une nouvelle édition papier? Numérique? Payante? A participation
libre? Gratuite en parallèle d’une dynamique associative pour trouver
des bras et quelques sous?

Laisser les topos « privés » récupérer le marché et trouver de
l’argent ailleurs pour financer l’entretien de nos très nombreuses
falaises, tout en conservant un accès libre et gratuit. Les nouvelles
économies vont redistribuer les cartes (les marchés du gratuit, les
plateformes numériques collaboratives ou pas, les réseaux sociaux,
etc…), de plus les conséquences de Vingrau ne sont pas encore visible,
on aura tout le temps de réfléchir à la sauvegarde de l’espace de
grimpe pour une pratique libre et gratuite (à noter que le gratuit ne
l’est jamais vraiment, c’est juste que quelqu’un paye toujours à votre
place : impôts, subventions, publicité, topos, etc…).

Tout cela va demander réflexion et réinvestisssement des grimpeurs car
les équipeurs/gestionnaires de nos falaises ne pourront peut être pas
prendre en charge les futures recherchent de financement bien plus
chronophage qu’un topo à faire tous les 10/15 ans, ni forcément
acquérir les nouvelles compétences numériques des évolutions de notre
monde. C’est à la nouvelle génération de construire son futur commun,
si besoin avec l’aide et les conseils des « anciens » éclairés de leur
expérience passée, pour soutenir des jeunes grimpeurs
visionnaires. L’avenir nous le dira.

Rémi Cappeau, le xx/yy/2020

Rémi fédère, depuis plus de 10 ans comme l’ont fait avant René Finet,
Alain Finet, Jean Marc Cagglini, Daniel Dupuis …, l’équipement et
l’entretien bénévole de la falaise d’Hauteroche. Chaque génération
d’équipeur a été aidé par beaucoup de grimpeurs conscients de
l’importance de ce travail. Un énorme travail de sécurisation de la
falaise, qui est unanimement apprécié par les pratiquants, qui fait
que 40 ans après sa découverte, la falaise reste très fréquentée.
