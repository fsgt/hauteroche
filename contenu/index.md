---
home: true
heroImage: https://cordee13.fr/wp-content/uploads/2023/01/Hauteroche-29-09-2018_ENTRETIEN-16.jpg
tagline: |
    La falaise de Hauteroche appartient à la commune de Hauteroche qui la “loue” à la FSGT depuis sa découverte en 1977.
    Elle est entièrement équipée et entretenue par des bénévoles, des milliers d’heures depuis 1977 !
actionText: Topo Hauteroche
actionLink: /falaise/topo-hauteroche
footer: Fait par des bénévoles FSGT avec ❤️
---
