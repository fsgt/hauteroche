const { mdEnhancePlugin } = require("vuepress-plugin-md-enhance");
const { description } = require('../../package')

module.exports = {
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#title
   */
  title: 'Falaise De Hauteroche',
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#description
   */
  description: description,
  base: '/hauteroche/',

  /**
   * Extra tags to be injected to the page HTML `<head>`
   *
   * ref：https://v1.vuepress.vuejs.org/config/#head
   */
  head: [
    ['meta', { name: 'theme-color', content: '#3eaf7c' }],
    ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
    ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }]
  ],

  /**
   * Theme configuration, here is the default theme configuration for VuePress.
   *
   * ref：https://v1.vuepress.vuejs.org/theme/default-theme-config.html
   */
  themeConfig: {
    repo: 'https://gitlab.com/fsgt/hauteroche',
    docsDir: 'contenu',
    editLinks: true,
    editLinkText: 'Modifier cette page',
    lastUpdated: false,
    nav: [
      {
        text: 'La falaise',
        link: '/falaise/',
      },
      {
        text: 'FSGT',
        link: 'https://www.fsgt.org'
      }
    ],
    sidebar: {
      '/falaise/': [
        {
          title: 'La Falaise',
          collapsable: false,
          children: [
            'topo-hauteroche',
            'financement-hauteroche',
            'temoignage-hauteroche',
            'historique-hauteroche'
          ]
        }
      ],
    }
  },

  /**
   * Apply plugins，ref：https://v1.vuepress.vuejs.org/zh/plugin/
   */
  plugins: [
    '@vuepress/plugin-back-to-top',
    '@vuepress/plugin-medium-zoom',
    ["md-enhance", {
      // Enable Footnote
      footnote: true,
    }],
  ],

  dest: 'public'
}
